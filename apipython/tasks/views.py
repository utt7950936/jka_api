from django.shortcuts import render
from rest_framework import generics, viewsets
from rest_framework.response import Response

from .models import *
from .serializer import *

# Create your views here.
#Aqui se importan los serializer

# Ejemplo de como realizar las demas
# class Nombredetabla+ViewSet(viewsets.ModelViewSet):
#     serializer_class = Nombre_del_Serializer
#     queryset = Nombre_del_modelo.objects.all()
    
    
class AdministradorViewSet(viewsets.ModelViewSet):
    serializer_class = AdministradorSerializer
    queryset = Administrador.objects.all()

class ConductoresViewSet(viewsets.ModelViewSet):
    serializer_class = ConductoresSerializer
    queryset = Conductores.objects.all()

class ConductoresTranspViewSet(viewsets.ModelViewSet):
    serializer_class = ConductoresTranspSerializer
    queryset = ConductoresTransp.objects.all()

class ConductoresTurnosViewSet(viewsets.ModelViewSet):
    serializer_class = ConductoresTurnosSerializer
    queryset = ConductoresTurnos.objects.all()

class DisponibilidadViewSet(viewsets.ModelViewSet):
    serializer_class = DisponibilidadSerializer
    queryset = Disponibilidad.objects.all()

class EmpleadosViewSet(viewsets.ModelViewSet):
    serializer_class = EmpleadosSerializer
    queryset = Empleados.objects.all()

class EmpleadosTranspViewSet(viewsets.ModelViewSet):
    serializer_class = EmpleadosTranspSerializer
    queryset = EmpleadosTransp.objects.all()

class MarcaViewSet(viewsets.ModelViewSet):
    serializer_class = MarcaSerializer
    queryset = Marca.objects.all()

class ModeloViewSet(viewsets.ModelViewSet):
    serializer_class = ModeloSerializer
    queryset = Modelo.objects.all()

class ParadasViewSet(viewsets.ModelViewSet):
    serializer_class = ParadasSerializer
    queryset = Paradas.objects.all()

class RutasViewSet(viewsets.ModelViewSet):
    serializer_class = RutasSerializer
    queryset = Rutas.objects.all()

class TransportesViewSet(viewsets.ModelViewSet):
    serializer_class = TransportesSerializer
    queryset = Transportes.objects.all()

class TurnosViewSet(viewsets.ModelViewSet):
    serializer_class = TurnosSerializer
    queryset = Turnos.objects.all()

class TurnosParadasViewSet(viewsets.ModelViewSet):
    serializer_class = TurnosParadasSerializer
    queryset = TurnosParadas.objects.all()
    
    #Filtrador/consulta para paradas de diferentes turnos
    
class FiltrarRutasAPIView(generics.ListAPIView):
    serializer_class = RutasSerializer
    
    def get_queryset(self):
            noruta = self.request.query_params.get('noruta')
            conductor = self.request.query_params.get('conductor')
            transporte = self.request.query_params.get('transporte')

            queryset = Rutas.objects.all()
            if noruta is not None:
                queryset = queryset.filter(noruta=noruta)
            if conductor is not None:
                queryset = queryset.filter(conductor=conductor)
            if transporte is not None:
                queryset = queryset.filter(transporte=transporte)

            return queryset