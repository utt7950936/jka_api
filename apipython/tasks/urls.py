from django.urls import include, path
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from tasks import views

from . import views

# api version

#Todo esto te genera lo siguiente:
#Get, post, Put, Delete
#Amo Python-django :)

#Ejemplo para solo una
    # router = routers.DefaultRouter()
    # router.register('tasks',views.AdministradorViewSet, 'tasks' )
    # #Se reciben 3 parametros
    # urlpatterns = [
    #     #Parametro 1: como es sitada la url
    #     path("api/v1", include(router.urls)) #Todas las peticiones que se pidan
    # ]


router = routers.DefaultRouter()

router.register('administrador', views.AdministradorViewSet, basename='administrador')
router.register('conductores', views.ConductoresViewSet, basename='conductores')
router.register('conductorestransp', views.ConductoresTranspViewSet, basename='conductorestransp')
router.register('conductoresturnos', views.ConductoresTurnosViewSet, basename='conductoreturnos')
router.register('disponibilidad', views.DisponibilidadViewSet, basename='disponibilidad')
router.register('empleados', views.EmpleadosViewSet, basename='empleados')
router.register('empleadostransp', views.EmpleadosTranspViewSet, basename='empleadostransp')
router.register('marca', views.MarcaViewSet, basename='marca')
router.register('modelo', views.ModeloViewSet, basename='modelo')
router.register('paradas', views.ParadasViewSet, basename='paradas')
router.register('rutas', views.RutasViewSet, basename='rutas')
router.register('transportes', views.TransportesViewSet, basename='transportes')
router.register('turnos', views.TurnosViewSet, basename='turnos')
router.register('turnosparadas', views.TurnosParadasViewSet, basename='turnosparadas')

urlpatterns = [
    path("api/v1/", include(router.urls)),
    path('docs/', include_docs_urls(title='Tasks API')),
    path('api/filtrar-rutas/', views.FiltrarRutasAPIView.as_view(), name='filtrar-rutas'),
]