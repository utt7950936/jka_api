import * as React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Image } from "expo-image";
import { FontFamily, FontSize, Color, Border, Padding } from "../GlobalStyles";

const DarkModeToggleContainer = ({
  iconLabel,
  featureLabel,
  notificationSettingsLabel,
}) => {
  return (
    <View style={styles.item}>
      <View style={styles.frame}>
        <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
          {iconLabel}
        </Text>
      </View>
      <View style={styles.titleParent}>
        <Text style={[styles.title, styles.titleFlexBox]}>{featureLabel}</Text>
        <Text style={[styles.subtitle, styles.titleFlexBox]}>
          {notificationSettingsLabel}
        </Text>
      </View>
      <Image
        style={[styles.itemChild, styles.iconPosition]}
        contentFit="cover"
        source={require("../assets/vector-2001.png")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  iconPosition: {
    overflow: "hidden",
    position: "absolute",
  },
  titleFlexBox: {
    textAlign: "left",
    fontFamily: FontFamily.robotoRegular,
    alignSelf: "stretch",
  },
  icon: {
    marginTop: -16,
    marginLeft: -16,
    top: "50%",
    left: "50%",
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    display: "flex",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
    overflow: "hidden",
    position: "absolute",
    height: 32,
    width: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  frame: {
    borderRadius: Border.br_base,
    backgroundColor: Color.colorGray_400,
    zIndex: 0,
    height: 32,
    width: 32,
  },
  title: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    color: Color.colorBlack,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    color: Color.colorGray_500,
  },
  titleParent: {
    flex: 1,
    zIndex: 1,
    marginLeft: 8,
  },
  itemChild: {
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    zIndex: 2,
    overflow: "hidden",
    position: "absolute",
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
  },
});

export default DarkModeToggleContainer;
