import React, { useMemo } from "react";
import { Text, StyleSheet, View } from "react-native";
import { Padding, Border, FontFamily, FontSize, Color } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const PasswordFormButtonContainer = ({
  resetPasswordBtnText,
  actionButtonText,
  propHeight,
  propHeight1,
}) => {
  const secondayStyle = useMemo(() => {
    return {
      ...getStyleValue("height", propHeight),
    };
  }, [propHeight]);

  const primaryStyle = useMemo(() => {
    return {
      ...getStyleValue("height", propHeight1),
    };
  }, [propHeight1]);

  return (
    <View style={styles.button}>
      <View style={[styles.seconday, styles.primaryFlexBox, secondayStyle]}>
        <Text style={[styles.title, styles.titleTypo]}>
          {resetPasswordBtnText}
        </Text>
      </View>
      <View style={[styles.primary, styles.primaryFlexBox, primaryStyle]}>
        <Text style={[styles.title1, styles.titleTypo]}>
          {actionButtonText}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  primaryFlexBox: {
    paddingVertical: Padding.p_3xs,
    justifyContent: "center",
    alignItems: "center",
    height: 64,
    borderRadius: Border.br_5xs,
    flex: 1,
    paddingHorizontal: Padding.p_xs,
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 22,
    fontSize: FontSize.size_base,
  },
  title: {
    color: Color.colorBlack,
  },
  seconday: {
    backgroundColor: Color.colorWhite,
    borderStyle: "solid",
    borderColor: Color.colorBlack,
    borderWidth: 1,
  },
  title1: {
    color: Color.colorWhite,
  },
  primary: {
    backgroundColor: Color.colorBlack,
    marginLeft: 8,
  },
  button: {
    alignSelf: "stretch",
    overflow: "hidden",
    flexDirection: "row",
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
});

export default PasswordFormButtonContainer;
