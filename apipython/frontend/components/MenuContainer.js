import * as React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image} from "react-native";
import { FontSize, FontFamily, Color, Padding } from "../GlobalStyles";
import { useNavigation } from '@react-navigation/native';


const MenuContainer = () => {
  const navigation = useNavigation(); 
  const irAtras = () => {
    navigation.goBack(); 
  };
  return (
    <View style={styles.topBar}>
      <View style={styles.topBar}>
      <View style={[styles.content, styles.buttonFlexBox]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>Menu</Text>
        <View style={styles.iconButtons} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    flex: 1,
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
  },
  iconButtons: {
    marginLeft: 8,
    alignItems: "center",
  },
  buttonFlexBox: {
    alignItems: "center",
    flexDirection: "row",
  },
  content: {
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_xs,
    paddingRight: Padding.p_5xs,
    alignSelf: "stretch",
    paddingBottom: Padding.p_xs,
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
});

export default MenuContainer;
