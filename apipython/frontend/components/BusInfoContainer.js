import React, { useMemo } from "react";
import { Text, StyleSheet, View, ImageSourcePropType } from "react-native";
import { Image } from "expo-image";
import { FontFamily, FontSize, Color, Border, Padding } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const BusInfoContainer = ({
  emojiIcon,
  vehicleModelName,
  busStopInfo,
  busScheduleDetails,
  busStopImageId,
  propFontWeight,
  propFontFamily,
}) => {
  const titleStyle = useMemo(() => {
    return {
      ...getStyleValue("fontWeight", propFontWeight),
      ...getStyleValue("fontFamily", propFontFamily),
    };
  }, [propFontWeight, propFontFamily]);

  return (
    <View style={styles.item}>
      <View style={styles.frame}>
        <Text style={[styles.icon, styles.iconPosition]} numberOfLines={1}>
          {emojiIcon}
        </Text>
      </View>
      <View style={styles.titleParent}>
        <Text style={[styles.title, styles.titleFlexBox, titleStyle]}>
          {vehicleModelName}
        </Text>
        <Text style={[styles.subtitle, styles.titleFlexBox]}>
          {busStopInfo}
        </Text>
      </View>
      <Text style={[styles.subtitle1, styles.titleTypo]}>
        {busScheduleDetails}
      </Text>
      <Image
        style={[styles.itemChild, styles.iconPosition]}
        contentFit="cover"
        source={busStopImageId}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  iconPosition: {
    overflow: "hidden",
    position: "absolute",
  },
  titleFlexBox: {
    textAlign: "left",
    alignSelf: "stretch",
  },
  titleTypo: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    color: Color.colorBlack,
  },
  icon: {
    marginTop: -16,
    marginLeft: -16,
    top: "50%",
    left: "50%",
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    display: "flex",
    color: Color.colorBlack,
    overflow: "hidden",
    fontFamily: FontFamily.robotoRegular,
    position: "absolute",
    height: 32,
    width: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  frame: {
    borderRadius: Border.br_base,
    backgroundColor: Color.colorGray_400,
    zIndex: 0,
    height: 32,
    width: 32,
  },
  title: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    color: Color.colorBlack,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    textAlign: "left",
  },
  titleParent: {
    flex: 1,
    zIndex: 1,
    marginLeft: 8,
  },
  subtitle1: {
    textAlign: "right",
    zIndex: 2,
    marginLeft: 8,
  },
  itemChild: {
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    zIndex: 3,
    overflow: "hidden",
    position: "absolute",
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
  },
});

export default BusInfoContainer;
