import * as React from "react";
import { Image } from "expo-image";
import { StyleSheet, Text, View } from "react-native";
import BusInfoContainer from "./BusInfoContainer";
import { Color, FontSize, Border, FontFamily, Padding } from "../GlobalStyles";

const ListContainer = () => {
  return (
    <View style={[styles.list, styles.listFlexBox]}>
      <BusInfoContainer
        emojiIcon="🚌"
        vehicleModelName="Mercedes Benz"
        busStopInfo="PARA-46-42"
        busScheduleDetails="Modelo: Torino"
        busStopImageId={require("../assets/vector-200.png")}
      />
      <View style={[styles.item, styles.listFlexBox]}>
        <Image
          style={styles.frameIcon}
          contentFit="cover"
          source={require("../assets/frame.png")}
        />
        <View style={styles.titleWrapper}>
          <Text style={[styles.title, styles.titleClr]}>Ruta</Text>
        </View>
        <Text style={[styles.subtitle, styles.titleClr]}>Zona Centro</Text>
        <Image
          style={styles.itemChild}
          contentFit="cover"
          source={require("../assets/vector-200.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
  },
  titleClr: {
    color: Color.colorBlack,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
  },
  frameIcon: {
    borderRadius: Border.br_base,
    width: 32,
    height: 32,
    zIndex: 0,
  },
  title: {
    fontFamily: FontFamily.robotoRegular,
    textAlign: "left",
    alignSelf: "stretch",
    color: Color.colorBlack,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
  },
  titleWrapper: {
    flex: 1,
    zIndex: 1,
    marginLeft: 8,
  },
  subtitle: {
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    textAlign: "right",
    zIndex: 2,
    marginLeft: 8,
  },
  itemChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    overflow: "hidden",
    maxHeight: "100%",
    zIndex: 3,
  },
  item: {
    flexDirection: "row",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
  },
  list: {
    height: 100,
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
    marginTop: 12,
  },
});

export default ListContainer;
