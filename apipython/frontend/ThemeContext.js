import React, { createContext, useContext, useState } from "react";

const ThemeContext = createContext();

// Hook personalizado para consumir el contexto
export const useTheme = () => useContext(ThemeContext);

// Proveedor de contexto para manejar el modo oscuro
export const ThemeProvider = ({ children }) => {
  const [isDarkMode, setIsDarkMode] = useState(false);

  const toggleTheme = () => {
    setIsDarkMode((prevMode) => !prevMode);
  };

  return (
    <ThemeContext.Provider value={{ isDarkMode, toggleTheme }}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContext;
