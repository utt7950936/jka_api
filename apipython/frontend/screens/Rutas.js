import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Color, FontFamily, Border, Padding, FontSize } from "../GlobalStyles";
import { useTheme } from "../ThemeContext";

const Rutas = () => {
  const navigation = useNavigation();
  const { isDarkMode } = useTheme();
  const irAtras = () => {
    navigation.goBack();
  };
  const CentroMat = () => {
    navigation.navigate("ZonaCentroMatutino");
  };
  const CentroVesp = () => {
    navigation.navigate("ZonaCentroVespertino");
  };
  const CentroNoct = () => {
    navigation.navigate("ZonaCentroNocturno");
  };
  const NatMat = () => {
    navigation.navigate("NaturaMatutino");
  };
  const NatVesp = () => {
    navigation.navigate("NaturaVespertino");
  };
  const NatNoct = () => {
    navigation.navigate("NaturaNocturno");
  };
  const VfMat = () => {
    navigation.navigate("VillaFontanaMatutino");
  };
  const VfVesp = () => {
    navigation.navigate("VillaFontanaVespertino");
  };
  const VfNoct = () => {
    navigation.navigate("VillaFontanaNocturno");
  };
  //Rutas

  const irARutaCentro = () => {
    navigation.navigate("RutaCentro");
  };

  const irARutaVilla = () => {
    navigation.navigate("RutaVillaFontana");
  };

  const irARutaNatura = () => {
    navigation.navigate("RutaNatura");
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;
  ///
  return (
    <ScrollView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.rutas}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.rowFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={imageSource}
              />
            </TouchableOpacity>
            <Text
              style={[
                styles.title,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Rutas
            </Text>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.rowFlexBox]}>
          <View style={styles.text}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Ruta 1
            </Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={styles.rowFlexBox}>
            <View style={styles.cardLayout}>
              <View style={styles.textContent}>
                <Text
                  style={[
                    styles.subtitle,
                    styles.titleTypo,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Zona Centro
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <TouchableOpacity onPress={irARutaCentro}>
                  <Image
                    style={[styles.image1Icon, styles.imageLayout]}
                    contentFit="cover"
                    source={require("../assets/Centro.png")}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
            <View style={[styles.card1, styles.cardLayout]}>
              <View style={styles.textContent}>
                <Text
                  style={[
                    styles.subtitle,
                    styles.titleTypo,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Paradas
                </Text>
              </View>
              <View
                style={[
                  isDarkMode ? styles.imageDark : styles.imageLight,
                  styles.imageLayout,
                ]}
              >
                <TouchableOpacity onPress={CentroMat}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Matutino
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={CentroNoct}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip1,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Nocturno
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={CentroVesp}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip2,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Vespertino
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.rowFlexBox]}>
          <View style={styles.text}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Ruta 2
            </Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={styles.rowFlexBox}>
            <View style={styles.cardLayout}>
              <View style={styles.textContent}>
                <Text
                  style={[
                    styles.subtitle,
                    styles.titleTypo,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Natura
                </Text>
              </View>
              <View style={styles.imageLayout}>
                <TouchableOpacity onPress={irARutaNatura}>
                  <Image
                    style={[styles.image1Icon, styles.imageLayout]}
                    contentFit="cover"
                    source={require("../assets/Natura.png")}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
            <View style={[styles.card1, styles.cardLayout]}>
              <View style={styles.textContent}>
                <Text
                  style={[
                    styles.subtitle,
                    styles.titleTypo,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Paradas
                </Text>
              </View>
              <View
                style={[
                  isDarkMode ? styles.imageDark : styles.imageLight,
                  styles.imageLayout,
                ]}
              >
                <TouchableOpacity onPress={NatMat}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Matutino
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={NatNoct}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip1,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Nocturno
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={NatVesp}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip2,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Vespertino
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.rowFlexBox]}>
          <View style={styles.text}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Ruta 3
            </Text>
          </View>
        </View>
        <View style={[styles.list, styles.listFlexBox]}>
          <View style={styles.rowFlexBox}>
            <View style={styles.cardLayout}>
              <View style={styles.textContent}>
                <Text
                  style={[
                    styles.subtitle,
                    styles.titleTypo,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Villa Fontana
                </Text>
              </View>
              <View style={[styles.image, styles.imageLayout]}>
                <TouchableOpacity onPress={irARutaVilla}>
                  <Image
                    style={[styles.image1Icon, styles.imageLayout]}
                    contentFit="cover"
                    source={require("../assets/Villa.png")}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
            <View style={[styles.card1, styles.cardLayout]}>
              <View style={styles.textContent}>
                <Text
                  style={[
                    styles.subtitle,
                    styles.titleTypo,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Paradas
                </Text>
              </View>
              <View
                style={[
                  isDarkMode ? styles.imageDark : styles.imageLight,
                  styles.imageLayout,
                ]}
              >
                <TouchableOpacity onPress={VfMat}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Matutino
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={VfNoct}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip1,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Nocturno
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={VfVesp}>
                  <View
                    style={[
                      isDarkMode ? styles.darkButton : styles.lightButton,
                      styles.chip2,
                      styles.chipLayout,
                    ]}
                  >
                    <Text
                      style={[
                        styles.text1,
                        isDarkMode ? styles.darkText : styles.lightText,
                      ]}
                    >
                      Vespertino
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.imageContainer} />
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  imageLight: {
    backgroundColor: Color.colorGray_400,
    height: 170,
    width: 148,
  },
  imageDark: {
    backgroundColor: Color.colorGray_300,
    height: 170,
    width: 148,
  },
  lightButton: {
    backgroundColor: "#d4d4d4", // Color de fondo para el modo claro
    // Otros estilos
  },

  // Estilo para el botón en modo oscuro
  darkButton: {
    backgroundColor: "#181818", // Color de fondo para el modo oscuro
    // Otros estilos
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff",
  },
  lightText: {
    color: "#000",
  },
  titleTypo: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  imageLayout: {
    height: 170,
    width: 148,
  },
  text1Typo: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  cardLayout: {
    height: 210,
    width: 150,
    borderWidth: 1,
    borderColor: Color.colorGray_500,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    overflow: "hidden",
    alignItems: "center",
  },
  chipLayout: {
    width: 85,
    left: 35,
    position: "absolute",
    padding: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    paddingHorizontal: Padding.p_3xs,
    textAlign: "left",
    alignSelf: "flex-start",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    marginTop: 12,
    alignItems: "center",
  },
  subtitle: {
    fontSize: FontSize.size_base,
    textAlign: "left",
    alignSelf: "stretch",
  },
  textContent: {
    padding: Padding.p_5xs,
    alignSelf: "stretch",
  },
  title2: {
    marginTop: -8,
    top: "50%",
    left: 16,
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    textAlign: "center",
    display: "flex",
    width: 116,
    height: 16,
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
  },
  imageContainer: {
    height: 150,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  text1: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    textAlign: "center",
  },
  chip: {
    top: 23,
  },
  chip1: {
    top: 111,
  },
  chip2: {
    top: 67,
  },
  card1: {
    marginLeft: 8,
  },
  list: {
    width: 335,
    paddingLeft: Padding.p_xs,
    paddingRight: Padding.p_base,
    marginTop: 12,
    overflow: "hidden",
  },
  rutas: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
});

export default Rutas;
