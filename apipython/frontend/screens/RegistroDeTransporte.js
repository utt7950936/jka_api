import React, { useState } from "react";
import { Image } from "react-native";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Button,
  Modal,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Padding, Color, FontFamily, FontSize, Border } from "../GlobalStyles";

const RegistroDeTransporte = () => {
  const [datosIngresados, setDatosIngresados] = useState([]);
  const [matricula, setMatricula] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [marca, setMarca] = useState("");
  const [modelo, setModelo] = useState("");

  const [mensajeError, setMensajeError] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [registroVerificado, setRegistroVerificado] = useState(false);

  const navigation = useNavigation();

  const irAtras = () => {
    navigation.goBack();
  };

  const RegTran = () => {
    // Verificar si hay campos vacíos
    if (
      matricula === "" ||
      descripcion === "" ||
      marca === "" ||
      modelo === ""
    ) {
      setMensajeError("Hay un campo vacío");
      setRegistroVerificado(false);
    } else {
      const nuevosDatos = [
        { campo: "Matricula", valor: matricula },
        { campo: "Descripcion", valor: descripcion },
        { campo: "Marca", valor: marca },
        { campo: "Modelo", valor: modelo },
      ];

      setDatosIngresados(nuevosDatos);
      setMensajeError("Transporte registrado");
      setRegistroVerificado(true);
    }

    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    setMatricula("");
    setDescripcion("");
    setMarca("");
    setModelo("");
    setMensajeError("");
  };

  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.registroDeTransporte}>
        <View style={[styles.content, styles.contentFlexBox]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Registro de transporte</Text>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Matricula</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={matricula}
              onChangeText={setMatricula}
              placeholder="Matricula del transporte"
              keyboardType="numeric"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Descripcion</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={descripcion}
              onChangeText={setDescripcion}
              placeholder="Descripcion del transporte"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Marca</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={marca}
              onChangeText={setMarca}
              placeholder="Marca del transporte"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={styles.title1}>Modelo</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={modelo}
              onChangeText={setModelo}
              placeholder="Modelo del transporte"
              keyboardType="default"
            />
          </View>
        </View>
        <Modal transparent={true} visible={modalVisible} animationType="slide">
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              {mensajeError !== "" && (
                <Text style={styles.errorText}>{mensajeError}</Text>
              )}
              {datosIngresados.length > 0 && (
                <View style={styles.listaContainer}>
                  <Text style={styles.listaTitle}></Text>
                  {datosIngresados.map((dato, index) => (
                    <Text key={index} style={styles.listaItem}>
                      {dato.campo}: {dato.valor}
                    </Text>
                  ))}
                </View>
              )}
              <View style={styles.buttonContainer12}>
                <Button
                  color="#000"
                  title="Aceptar"
                  onPress={() => {
                    if (registroVerificado == true) {
                      irAtras();
                      closeModal(); // Llamar la función para cerrar el modal
                    } else {
                      closeModal(); // Llamar la función para cerrar el modal
                    }
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={RegTran} style={styles.button1}>
            <Text style={styles.buttonText1}>Registrar empleado</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  inputSpaceBlock: {
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textTypo: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    overflow: "hidden",
    flex: 1,
  },
  buttonText1: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    marginLeft: 8,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  button1: {
    backgroundColor: "#000",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    alignSelf: "stretch",
  },
  text: {
    height: 20,
  },
  buttonContainer: {
    paddingTop: 14,
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  textfield: {
    borderRadius: Border.br_7xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    marginTop: 4,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    justifyContent: "center",
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
  },
  registroDeTransporte: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default RegistroDeTransporte;
