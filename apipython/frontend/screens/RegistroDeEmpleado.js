import React, { useState } from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Button,
  Modal,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Padding, FontSize, Color, FontFamily, Border } from "../GlobalStyles";

const RegistroDeEmpleado = () => {
  const [datosIngresados, setDatosIngresados] = useState([]);
  const [empleado, setEmpleado] = useState("");
  const [primApe, setPrimerApe] = useState("");
  const [seguApe, setSegunApe] = useState("");
  const [calle, setCalle] = useState("");
  const [numExt, setNumExt] = useState("");
  const [colonia, setColonia] = useState("");
  const [numTel, setNumTel] = useState("");
  const [mensajeError, setMensajeError] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [registroVerificado, setRegistroVerificado] = useState(false);

  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  const RegEmp = () => {
    // Verificar si hay campos vacíos
    if (
      empleado === "" ||
      primApe === "" ||
      calle === "" ||
      numExt === "" ||
      colonia === "" ||
      numTel === ""
    ) {
      setMensajeError("Hay un campo vacío");
      setRegistroVerificado(false);
    } else {
      // Formatear el número de teléfono antes de agregarlo a la lista de datos ingresados
      const telefonoFormateado = formatTelefono(numTel);

      // Agregar todos los datos ingresados a la lista
      const nuevosDatos = [
        { campo: "Empleado", valor: empleado },
        { campo: "Primer Apellido", valor: primApe },
        { campo: "Segundo Apellido", valor: seguApe },
        { campo: "Calle", valor: calle },
        { campo: "Número exterior", valor: numExt },
        { campo: "Colonia", valor: colonia },
        { campo: "Número de teléfono", valor: telefonoFormateado },
      ];

      // Actualizar el estado con los nuevos datos ingresados
      setDatosIngresados(nuevosDatos);
      setMensajeError("Empleado registrado");
      setRegistroVerificado(true);
    }

    // Mostrar el modal
    setModalVisible(true);
  };
  const formatTelefono = (input) => {
    let cleaned = ("" + input).replace(/\D/g, "");

    let formatted = "";
    if (cleaned.length > 0) {
      formatted += "(" + cleaned.substring(0, 3);
    }
    if (cleaned.length >= 3) {
      formatted += ") ";
      if (cleaned.length === 3 && input[input.length - 1] !== ")") {
        formatted += cleaned.substring(3);
      } else {
        formatted += cleaned.substring(3, 6);
      }
    }
    if (cleaned.length > 6) {
      formatted += "-" + cleaned.substring(6, 10);
    }
    return formatted;
  };

  const handleChangeTelefono = (input) => {
    setNumTel(formatTelefono(input)); // Corregido de setTelefono a setNumTel
  };

  const closeModal = () => {
    setModalVisible(false);
    setEmpleado("");
    setPrimerApe("");
    setSegunApe("");
    setCalle("");
    setNumExt("");
    setColonia("");
    setNumTel("");
    setMensajeError("");
  };

  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.registroDeEmpleado}>
        <View style={[styles.content]}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Registro de empleado</Text>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Nombre de pila</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={empleado}
              onChangeText={setEmpleado}
              placeholder="Nombre del empleado"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Primer Apellido</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={primApe}
              onChangeText={setPrimerApe}
              placeholder="Primer apellido del empleado"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Segundo Apellido</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={seguApe}
              onChangeText={setSegunApe}
              placeholder="Segundo apellido del empleado"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Calle</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={calle}
              onChangeText={setCalle}
              placeholder="Calle del domicilio"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Número exterior</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={numExt}
              onChangeText={setNumExt}
              placeholder="Numero exterior del domicilio"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>Colonia</Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={colonia}
              onChangeText={setColonia}
              placeholder="Colonia del domicilio"
              keyboardType="default"
            />
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textTypo]}>
            Número de teléfono
          </Text>
          <View style={[styles.textfield, styles.inputSpaceBlock]}>
            <TextInput
              style={[styles.text1, styles.text1Typo]}
              value={numTel}
              onChangeText={handleChangeTelefono}
              placeholder="Numero de telefono del empleado"
              keyboardType="numeric"
            />
          </View>
        </View>
        <Modal transparent={true} visible={modalVisible} animationType="slide">
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              {mensajeError !== "" && (
                <Text style={styles.errorText}>{mensajeError}</Text>
              )}
              {datosIngresados.length > 0 && (
                <View style={styles.listaContainer}>
                  <Text style={styles.listaTitle}></Text>
                  {datosIngresados.map((dato, index) => (
                    <Text key={index} style={styles.listaItem}>
                      {dato.campo}: {dato.valor}
                    </Text>
                  ))}
                </View>
              )}
              <View style={styles.buttonContainer12}>
                <Button
                  color="#000"
                  title="Aceptar"
                  onPress={() => {
                    if (registroVerificado == true) {
                      irAtras();
                      closeModal(); // Llamar la función para cerrar el modal
                    } else {
                      closeModal(); // Llamar la función para cerrar el modal
                    }
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>
        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={RegEmp} style={styles.button1}>
            <Text style={styles.buttonText1}>Registrar empleado</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "white",
    padding: 20,
    borderRadius: 10,
  },
  modalTitle: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: "center",
  },
  buttonText1: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  button1: {
    backgroundColor: "#000",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  buttonContainer: {
    paddingTop: 14,
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  buttonContainer12: {
    paddingTop: 14,
    flexDirection: "row",
    justifyContent: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  inputSpaceBlock: {
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textTypo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    marginLeft: 8,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  text: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorGray_500,
    height: 20,
    overflow: "hidden",
    fontSize: FontSize.size_sm,
    flex: 1,
  },
  textfield: {
    borderRadius: Border.br_7xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    marginTop: 4,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    justifyContent: "center",
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
  },
  registroDeEmpleado: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default RegistroDeEmpleado;
