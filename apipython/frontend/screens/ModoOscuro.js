import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Switch,
  Text,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import { useTheme } from "../ThemeContext";
import { useNavigation } from "@react-navigation/native";
import { ThemeProvider } from "../ThemeContext";

import { Padding, FontFamily, Color, FontSize } from "../GlobalStyles";

const ModoOscuro = () => {
  const navigation = useNavigation();

  const { isDarkMode, toggleTheme } = useTheme();
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;
  const irAtras = () => {
    navigation.goBack();
  };
  return (
    <ThemeProvider>
      <View style={isDarkMode ? styles.darkBackground : styles.safeArea}>
        <View style={styles.Oscuro}>
          <View style={styles.topBar}>
            <View style={[styles.content, styles.rowFlexBox]}>
              <TouchableOpacity onPress={irAtras}>
                <Image
                  style={styles.icLeftIcon}
                  contentFit="cover"
                  source={imageSource}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.title,
                  styles.titleTypo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Configuración
              </Text>
            </View>
          </View>
          <View style={styles.switchContainer}>
            <Text style={isDarkMode ? styles.darkText : styles.lightText}>
              Modo Oscuro
            </Text>
            <Switch value={isDarkMode} onValueChange={toggleTheme} />
          </View>
          <View style={{ marginHorizontal: 30 }}>
            <Text style={isDarkMode ? styles.darkText : styles.lightText}>
              Actívalo con un toque y disfruta de una experiencia visual más
              cómoda en entornos de poca luz.
            </Text>
          </View>
        </View>
      </View>
    </ThemeProvider>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
  },
  Oscuro: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  lightBackground: {
    backgroundColor: Color.colorWhite,
  },
  switchContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  darkText: {
    color: "#fff",
  },
  lightText: {
    color: "#000",
  },
});

export default ModoOscuro;
