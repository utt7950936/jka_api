import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";
import { FontSize, FontFamily, Color, Padding } from "../GlobalStyles";
import { useTheme } from "../ThemeContext";

const CerrarSesion = () => {
  const { isDarkMode } = useTheme();
  const navigation = useNavigation();

  const irAtras = () => {
    navigation.goBack();
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;

  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.prox}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.rowFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={imageSource}
              />
            </TouchableOpacity>
            <Text
              style={[
                styles.title,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Proximamente
            </Text>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.confirmacionCierreDeSesion}>
            <View style={[styles.card, styles.cardBorder]}>
              <Text
                style={[
                  styles.subtitle,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                PROXIMAMENTE...
              </Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  topBar: {
    alignSelf: "stretch",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  prox: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  confirmacionCierreDeSesion: {
    width: "80%",
    maxWidth: 300,
  },
  card: {
    padding: 20,
    borderRadius: 10,
  },
  cardBorder: {
    borderWidth: 1,
    borderColor: "#ccc",
  },
  subtitle: {
    fontSize: 18,
    textAlign: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    alignItems: "center",
  },
  secondary: {
    backgroundColor: "#000",
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  titleTypo: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
});

export default CerrarSesion;
