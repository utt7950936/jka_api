import React, { useState } from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Modal,
  FlatList,
  Button,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import RealTimeLocationMapContainer from "../components/RealTimeLocationMapContainer";
import { FontFamily, Color, Padding, FontSize, Border } from "../GlobalStyles";
import DateTimePicker from "@react-native-community/datetimepicker";

const CreacionDeRutas = () => {
  const navigation = useNavigation();
  const [selectedTransport, setSelectedTransport] = useState(null);
  const [selectedDriver, setSelectedDriver] = useState(null);
  const [selectedTurno, setSelectedTurno] = useState(null);
  const [routeName, setRouteName] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [paradas, setParadas] = useState([]);
  const [selectedTime, setSelectedTime] = useState(new Date());
  const [nombreParada, setNombreParada] = useState("");
  const [showTimePicker, setShowTimePicker] = useState(false);

  const irAtras = () => {
    navigation.goBack();
  };

  const handleTransportSelection = (transport) => {
    setSelectedTransport(transport);
  };

  const handleDriverSelection = (driver) => {
    setSelectedDriver(driver);
  };

  const handleTurnoSelection = (Turno) => {
    setSelectedTurno(Turno);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const agregarParada = () => {
    const nuevaParada = {
      nombre: nombreParada || `Parada ${paradas.length + 1}`,
      hora: selectedTime,
    };
    setParadas([...paradas, nuevaParada]);
    closeModal();
  };

  const handleTimeChange = (event, selectedTime) => {
    setShowTimePicker(false);
    if (selectedTime) {
      setSelectedTime(selectedTime);
    }
  };

  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.creacionDeRutas}>
        <View style={styles.content}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={[styles.title, styles.titleTypo]}>
            Creación de rutas
          </Text>
          <View style={styles.iconButtons} />
        </View>
        <View style={[styles.input, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Nombre de ruta</Text>
          <View style={[styles.textfield, styles.textfieldFlexBox]}>
            <TextInput
              style={[styles.text, styles.textTypo]}
              value={routeName}
              onChangeText={setRouteName}
              placeholder="Nombre de la ruta"
            />
          </View>
        </View>
        <View style={[styles.selection, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Asignar transporte</Text>
          <View style={styles.textfieldFlexBox}>
            <FlatList
              horizontal
              data={["Torino", "Charger", "Bochito"]}
              keyExtractor={(item) => item}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => handleTransportSelection(item)}
                  style={[
                    styles.chipFlexBox,
                    selectedTransport === item && {
                      backgroundColor: Color.colorPrimary,
                    },
                  ]}
                >
                  <Text
                    style={[
                      styles.text1,
                      styles.textTypo,
                      selectedTransport === item && {
                        color: Color.colorWhite,
                      },
                    ]}
                  >
                    {item}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
        <View style={[styles.selection, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Conductor</Text>
          <FlatList
            horizontal
            data={[
              "Valentina Campos",
              "Juan Pérez",
              "María García",
              "Jordan Vidana",
            ]}
            keyExtractor={(item) => item}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => handleDriverSelection(item)}
                style={[
                  styles.chipFlexBox,
                  selectedDriver === item && {
                    backgroundColor: Color.colorPrimary,
                  },
                ]}
              >
                <Text
                  style={[
                    styles.text1,
                    styles.textTypo,
                    selectedDriver === item && {
                      color: Color.colorWhite,
                    },
                  ]}
                >
                  {item}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View style={[styles.selection, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Turno</Text>
          <View style={styles.textfieldFlexBox}>
            <FlatList
              horizontal
              data={["Matutino", "Vespertino", "Nocturno"]}
              keyExtractor={(item) => item}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => handleTurnoSelection(item)}
                  style={[
                    styles.chipFlexBox,
                    selectedTurno === item && {
                      backgroundColor: Color.colorPrimary,
                    },
                  ]}
                >
                  <Text
                    style={[
                      styles.text1,
                      styles.textTypo,
                      selectedTurno === item && {
                        color: Color.colorWhite,
                      },
                    ]}
                  >
                    {item}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>

        <RealTimeLocationMapContainer
          imageAltText={require("../assets/image1.png")}
          locationDescription="Ruta en el mapa"
        />

        <View style={[styles.sectionTitle, styles.listSpaceBlock]}>
          <View style={styles.text10}>
            <Text style={[styles.title5, styles.titleTypo]}>Paradas</Text>
          </View>
        </View>
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <TouchableOpacity onPress={openModal}>
            <Text style={{ fontSize: 18, marginBottom: 10 }}>
              Agregar Parada
            </Text>
          </TouchableOpacity>

          {/* Modal */}
          <Modal
            transparent={true}
            visible={modalVisible}
            animationType="slide"
          >
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "rgba(0, 0, 0, 0.5)",
              }}
            >
              <View
                style={{
                  backgroundColor: "white",
                  padding: 20,
                  borderRadius: 10,
                }}
              >
                <Text style={{ fontSize: 20, marginBottom: 20 }}>
                  Agregar Parada
                </Text>
                <TextInput
                  placeholder="Nombre de la parada"
                  style={{
                    marginBottom: 10,
                    borderBottomWidth: 1,
                    borderColor: "#ccc",
                  }}
                  value={nombreParada}
                  onChangeText={(text) => setNombreParada(text)}
                />
                <TouchableOpacity onPress={() => setShowTimePicker(true)}>
                  <Text style={{ fontSize: 16, color: "blue" }}>
                    Seleccionar Hora
                  </Text>
                </TouchableOpacity>
                {showTimePicker && (
                  <DateTimePicker
                    value={selectedTime}
                    mode="time"
                    is24Hour={true}
                    display="default"
                    onChange={handleTimeChange}
                  />
                )}
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 20,
                  }}
                >
                  <Button title="Guardar" onPress={agregarParada} />
                  <Button title="Cerrar" onPress={closeModal} />
                </View>
              </View>
            </View>
          </Modal>

          {/* Mostrar Paradas */}
          <View style={{ marginTop: 20 }}>
            <Text style={{ fontSize: 20, marginBottom: 10 }}>Paradas:</Text>
            {paradas.map((parada, index) => (
              <Text key={index}>{`${
                parada.nombre
              } - ${parada.hora.toLocaleTimeString()}`}</Text>
            ))}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    color: Color.colorBlack,
  },
  listSpaceBlock: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textfieldFlexBox: {
    marginTop: 4,
    flexDirection: "row",
    alignSelf: "stretch",
  },
  textTypo: {
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  chipFlexBox: {
    padding: Padding.p_5xs,
    backgroundColor: Color.colorGray_400,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
  },
  chipLayout: {
    width: 90,
    padding: Padding.p_5xs,
    backgroundColor: Color.colorGray_400,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  iconButtons: {
    marginLeft: 8,
    alignItems: "center",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    alignSelf: "stretch",
  },
  text: {
    color: Color.colorGray_500,
    height: 20,
    overflow: "hidden",
    flex: 1,
  },
  textfield: {
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    marginTop: 4,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  input: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    justifyContent: "center",
    overflow: "hidden",
  },
  text1: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
  },
  chip1: {
    marginLeft: 8,
  },
  selection: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
    alignItems: "center",
  },
  chip7: {
    marginLeft: 8,
  },
  title5: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text10: {
    flex: 1,
  },
  sectionTitle: {
    paddingTop: Padding.p_base,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignItems: "center",
  },
  list: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    justifyContent: "center",
    alignItems: "center",
  },
  creacionDeRutas: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
  showTimePickerButton: {
    marginTop: 8,
    color: Color.colorPrimary,
    fontFamily: FontFamily.robotoMedium,
    fontSize: FontSize.size_sm,
  },
});

export default CreacionDeRutas;
