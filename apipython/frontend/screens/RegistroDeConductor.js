import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import PasswordFormButtonContainer from "../components/PasswordFormButtonContainer";
import ErrorReportingContainer from "../components/ErrorReportingContainer";
import { FontFamily, Padding, FontSize, Color, Border } from "../GlobalStyles";

const RegistroDeConductor = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.registroDeConductor}>
        <View style={styles.content}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={[styles.title, styles.titleTypo]}>
            Registro de conductor
          </Text>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textLayout]}>Nombre de pila</Text>
          <View style={[styles.textfield, styles.imageLayout]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su nombre
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textLayout]}>
            Primer Apellido
          </Text>
          <View style={[styles.textfield, styles.imageLayout]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su primer apellido
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textLayout]}>
            Segundo Apellido
          </Text>
          <View style={[styles.textfield, styles.imageLayout]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su segundo apellido (opcional)
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textLayout]}>Edad</Text>
          <View style={[styles.textfield, styles.imageLayout]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su edad
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <Text style={[styles.title1, styles.textLayout]}>
            Número de teléfono
          </Text>
          <View style={[styles.textfield, styles.imageLayout]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Introduzca su número de teléfono
            </Text>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.inputSpaceBlock]}>
          <View style={styles.text5}>
            <Text style={[styles.title6, styles.titleTypo]}>
              Subir documentos
            </Text>
            <Text style={[styles.subtitle, styles.textTypo]}>
              Suba los siguientes documentos:
            </Text>
          </View>
        </View>
        <View style={[styles.imageContainer, styles.inputSpaceBlock]}>
          <View style={[styles.image, styles.imageLayout]}>
            <Text style={styles.title7}>Tarjeta de identidad</Text>
          </View>
        </View>
        <View style={[styles.imageContainer, styles.inputSpaceBlock]}>
          <View style={[styles.image, styles.imageLayout]}>
            <Text style={styles.title7}>Licencia de conducir</Text>
          </View>
        </View>
        <PasswordFormButtonContainer
          resetPasswordBtnText="Cancelar"
          actionButtonText="Registrar"
          propHeight="unset"
          propHeight1="unset"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
  },
  inputSpaceBlock: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textLayout: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
  },
  imageLayout: {
    borderRadius: Border.br_7xs,
    alignSelf: "stretch",
  },
  textTypo: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    textAlign: "left",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    lineHeight: 24,
    textAlign: "left",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    alignSelf: "stretch",
  },
  text: {
    height: 20,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    overflow: "hidden",
    flex: 1,
  },
  textfield: {
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    marginTop: 4,
    paddingHorizontal: Padding.p_xs,
    borderRadius: Border.br_7xs,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
    justifyContent: "center",
  },
  title6: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    textAlign: "left",
    alignSelf: "stretch",
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    alignSelf: "stretch",
  },
  text5: {
    flex: 1,
  },
  sectionTitle: {
    paddingTop: Padding.p_base,
    flexDirection: "row",
    alignItems: "center",
  },
  title7: {
    position: "absolute",
    marginTop: -8,
    top: "50%",
    left: 16,
    fontSize: FontSize.size_base,
    lineHeight: 22,
    textAlign: "center",
    display: "flex",
    width: 304,
    height: 16,
    justifyContent: "center",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    alignItems: "center",
  },
  image: {
    backgroundColor: Color.colorGray_400,
    flex: 1,
  },
  imageContainer: {
    height: 360,
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
    flexDirection: "row",
  },
  registroDeConductor: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default RegistroDeConductor;
