import React, { useState, useEffect } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";
import axios from "axios";
import { useTheme } from "../ThemeContext";
export default function App() {
  const { isDarkMode } = useTheme();
  const navigation = useNavigation();
  const [image, setImage] = useState("");
  const [error, setError] = useState("");
  const [ErrorLength, setErrorLength] = useState(0);

  const handleChangeError = (input) => {
    setError(input);
    setErrorLength(input.length);
  };

  const irAtras = () => {
    navigation.goBack();
  };

  const enviarError = async () => {
    try {
      const formData = new FormData();
      formData.append("title", "Error reportado desde la aplicación móvil");
      formData.append("description", error);
      if (image) {
        const uriParts = image.split(".");
        const fileType = uriParts[uriParts.length - 1];
        formData.append("file", {
          uri: image,
          name: `photo.${fileType}`,
          type: `image/${fileType}`,
        });
      }

      const response = await axios.post(
        `https://gitlab.com/api/v4/projects/55244746/issues`,
        formData,
        {
          headers: {
            "Private-Token": "glpat-rpA6swCgrym-TrQs5Wzk",
            "Content-Type": "multipart/form-data",
          },
        }
      );
      console.log("Issue creado con éxito:", response.data);
      // Aquí puedes agregar lógica adicional, como mostrar un mensaje de éxito al usuario
    } catch (error) {
      console.error("Error al crear el issue:", error);
      // Aquí puedes manejar el error de manera adecuada, por ejemplo, mostrando un mensaje de error al usuario
    }
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;

  return (
    <ScrollView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.reporteDeErrores}>
        <View style={[styles.content, styles.contentFlexBox]}>
          <TouchableOpacity onPress={irAtras}>
            <Image contentFit="cover" source={imageSource} />
          </TouchableOpacity>
          <Text
            style={[
              styles.title,
              styles.titleTypo,
              isDarkMode ? styles.darkText : styles.lightText,
            ]}
          >
            Reporte de errores
          </Text>
        </View>
        <View style={[styles.sectionTitle, styles.inputSpaceBlock]}>
          <View style={styles.text}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Descripción de error
            </Text>
          </View>
        </View>
        <View style={[styles.input, styles.inputSpaceBlock]}>
          <View style={[styles.textfield, styles.textfieldBorder]}>
            <TextInput
              style={[
                isDarkMode ? styles.darkText : styles.lightText,

                styles.text1,
                styles.text1Typo,
                {
                  minHeight: Math.max(120, 20 + 12 * error.split("\n").length),
                },
              ]}
              placeholder="Escribe tu error aquí. Por favor, sea lo más específico posible."
              placeholderTextColor={
                isDarkMode ? styles.darkText.color : styles.lightText.color
              }
              value={error}
              onChangeText={handleChangeError}
              maxLength={200}
              multiline
            />
          </View>
          <Text
            style={[
              styles.info,
              {
                color: isDarkMode
                  ? ErrorLength >= 180
                    ? "red"
                    : "lightgray"
                  : ErrorLength >= 180
                  ? "red"
                  : "black",
              },
            ]}
          >
            Caracteres restantes: {200 - ErrorLength}
          </Text>
        </View>
        <View style={[styles.container, styles.inputSpaceBlock]}>
          <TouchableOpacity
            onPress={enviarError}
            style={[styles.button1, { marginTop: 10 }]}
          >
            <Text style={styles.buttonText1}>Enviar error</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
  container: {
    flex: 1,
  },
  button1: {
    backgroundColor: "#000",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000",
  },
  buttonText1: {
    color: "#fff",
    fontSize: 16,
    textAlign: "center",
  },
  imageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    flex: 1,
    width: "100%",
    height: "100%",
    resizeMode: "contain",
    alignSelf: "stretch",
  },

  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 20,
  },
  button: {
    backgroundColor: "#007bff",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
  },
  image: {
    aspectRatio: 1, // Para mantener la proporción de aspecto
    resizeMode: "contain", // Para ajustar la imagen al tamaño del contenedor
    borderRadius: 10,
    alignSelf: "center",
    minWidth: 400, // Establecer un tamaño mínimo para el contenedor de la imagen
    minHeight: 400,
  },
  noImageText: {
    fontSize: 16,
    color: "gray",
    textAlign: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  titleTypo: {
    textAlign: "left",
    lineHeight: 24,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
  },
  inputSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  infoClr: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
  },
  text1Typo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  infoSpaceBlock: {
    marginTop: 4,
    alignSelf: "stretch",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  subtitle: {
    lineHeight: 16,
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignItems: "center",
  },
  title2: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  text1: {
    fontFamily: FontFamily.robotoRegular,
    height: 20,
    overflow: "hidden",
    flex: 1,
    verticalAlign: "top",
  },
  textfield: {
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignItems: "center",
  },
  info: {
    color: Color.colorGray_500,
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 16,
    fontSize: FontSize.size_xs,
    textAlign: "left",
  },
  input: {
    justifyContent: "center",
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
  },
  imageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  selectedImageContainer: {
    marginTop: 20,
    alignItems: "center",
  },
  selectedImageText: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 10,
  },
  selectedImage: {
    width: 200,
    height: 200,
  },
  reporteDeErrores: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});
