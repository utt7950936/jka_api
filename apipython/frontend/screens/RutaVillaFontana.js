import React, { Component } from "react";
import { Dimensions, View } from "react-native";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 32.51197725500379;
const LONGITUDE = -116.86581874363792;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = "AIzaSyBuAjMS_UH8pCAJKcpxTwkbzg6iItU4xMc";

class RutaVillaFontana extends Component {
  constructor(props) {
    super(props);

    this.state = {
      coordinates: [
        { latitude: 32.51197725500379, longitude: -116.86581874363792 },
        { latitude: 32.5036783966454, longitude: -116.86419026608459 },
        { latitude: 32.48937696778556, longitude: -116.82787150591855 },
        { latitude: 32.48067699338602, longitude: -116.82070164029172 },
        { latitude: 32.471436343747115, longitude: -116.84253994328628 },
        { latitude: 32.473240260209224, longitude: -116.84563330078544 },
        { latitude: 32.47360313660944, longitude: -116.84714145465186 },
        { latitude: 32.47299357050736, longitude: -116.85226041926636 },
        { latitude: 32.469487351161476, longitude: -116.85041149717104 },
        { latitude: 32.45399998188011, longitude: -116.88099210803543 },
      ],
      stopNames: [
        "Inicio, Villa Fontana",
        "Av. Aranjuez",
        "Farmacia Roma, Terrazas",
        "Calle Amaranto",
        "Haemonetics",
      ],
      omittedMarkers: [3, 4, 5, 6, 7],
    };

    this.mapView = null;
  }

  onReady = (result) => {
    this.mapView.fitToCoordinates(result.coordinates, {
      edgePadding: {
        right: width / 10,
        bottom: height / 10,
        left: width / 10,
        top: height / 10,
      },
    });
  };

  render() {
    const { coordinates, stopNames, omittedMarkers } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <MapView
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
          style={{ flex: 1 }}
          ref={(c) => (this.mapView = c)}
        >
          <MapViewDirections
            origin={coordinates[0]}
            destination={coordinates[coordinates.length - 1]}
            waypoints={coordinates.slice(1, -1)}
            mode="DRIVING"
            apikey={GOOGLE_MAPS_APIKEY}
            language="en"
            strokeWidth={4}
            strokeColor="#5065F6"
            onStart={(params) => {
              console.log(
                `Started routing between "${params.origin}" and "${
                  params.destination
                }"${
                  params.waypoints.length
                    ? " using waypoints: " + params.waypoints.join(", ")
                    : ""
                }`
              );
            }}
            onReady={this.onReady}
            onError={(errorMessage) => {
              console.log(errorMessage);
            }}
            resetOnChange={false}
          />

          {coordinates.map((coordinate, index) => {
            const adjustedIndex =
              index - omittedMarkers.filter((marker) => marker < index).length;
            if (!omittedMarkers.includes(index)) {
              return (
                <Marker
                  key={index}
                  coordinate={coordinate}
                  title={
                    stopNames[adjustedIndex] ? stopNames[adjustedIndex] : ""
                  }
                />
              );
            } else {
              return null;
            }
          })}
        </MapView>
      </View>
    );
  }
}

export default RutaVillaFontana;
