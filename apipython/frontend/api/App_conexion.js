// import axios from 'axios';
// import React from 'react';

// class App_conexion extends React.Component {

//   state = {details: [], }

//   componentDidMount() {

//     let data;
//     axios.get('http://127.0.0.1:8000/tasks/api/v1/rutas/')
//   .then(res => {
//     data = res.data;
//     this.setState({
//       details: data
//     });
//   })
//   .catch(err => { })
  
//   }

//   render() {
//     return (
//       <div>
//       <header>Data Generated from django</header>
//       <hr></hr>
//       {this.state.details.map((output, id)=> (
//         <div key={id}>
//         <div>
//           <h2>{output.noruta}</h2>
//           <h3>{output.nombre}</h3>
//           <h3>{output.hora}</h3>

//           </div>
//         </div>
//       ))}
//       </div>
//       )
//   }



// }

// export default App_conexion;

import axios from 'axios';
import React, { useEffect, useState } from 'react';

const App_conexion = ({ tableName, render }) => {
  const [details, setDetails] = useState([]);

  useEffect(() => {
    fetchData(tableName);
  }, [tableName]);

  const fetchData = (tableName) => {
    axios.get(`http://127.0.0.1:8000/tasks/api/v1/${tableName}/`)
      .then(res => {
        setDetails(res.data);
      })
      .catch(err => {
        console.error('Error fetching data:', err);
      });
  }

  return (
    <div>
      {render(details)}
      
    </div>
  );
}

export default App_conexion;

