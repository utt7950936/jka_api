import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Rutas from './components/Rutas';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>¡Comienza a trabajar en tu aplicación!</Text>
      <Rutas /> {/* Renderiza el componente de las rutas */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
