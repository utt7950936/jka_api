import axios from 'axios';
import React from 'react';
import { Modal, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import App_conexion from '../api/App_conexion';


class Rutas extends React.Component {
    state = {
        showModal: false, //Estado de modal para ver mas
        showModalEdit: false, // Agregamos un estado para el modal de edición
        selectedRoute: null,
        editedRoute: {},
        routes: [],

        routeToAdd: { // Nuevo estado para almacenar los datos de la nueva ruta
            noruta: '',
            nombre: '',
            hora: '',
            fechainicio: '',
            fechafinal: '',
            conductor: '',
            transporte: '',
            parada: '',
    }
};


// Función para manejar el evento de guardado de la nueva ruta
handleSaveNewRoute = async () => {
    const { routeToAdd } = this.state;

    try {
        // Guardar los datos de la nueva ruta
        const response = await axios.post('http://127.0.0.1:8000/tasks/api/v1/rutas/', routeToAdd);
        console.log('Nueva ruta guardada correctamente:', response.data);

        // Actualizar el estado local con la nueva ruta
        this.setState(prevState => ({
            routes: [...prevState.routes, response.data],
            showModalAdd: false, // Cerrar el modal de creación
            routeToAdd: { // Reiniciar los valores del formulario
                noruta: '',
                nombre: '',
                hora: '',
                fechainicio: '',
                fechafinal: '',
                conductor: '',
                transporte: '',
                parada: ''
            }
        }));
    } catch (error) {
        console.error('Error al guardar la nueva ruta:', error);
    }
};


// Función para cerrar el modal de creación de ruta
closeModalAdd = () => {
    this.setState({ showModalAdd: false });
};

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.heading}>Detalles de las rutas:</Text>
                    <TouchableOpacity style={styles.addButton} onPress={this.handleAddRoute}>
                        <Text style={styles.addButtonText}>Anadir ruta nueva</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.table}>
                        <View style={styles.tableRow}>
                            <Text style={[styles.cell, styles.headerCell]}>Número de Ruta</Text>
                            <Text style={[styles.cell, styles.headerCell]}>Nombre de la Ruta</Text>
                            <Text style={[styles.cell, styles.headerCell]}>Acciones</Text>
                        </View>
                        <App_conexion tableName="rutas" render={this.renderTable} />
                    </View>
                </ScrollView>

                {/* Codigo para la parte de ver mas */}
                {this.state.selectedRoute && (
                    <Modal
                        visible={this.state.showModal}
                        transparent={true}
                        animationType="slide" >
                        <View style={styles.modalContainer}>
                            <ScrollView style={styles.modalContent}>
                                <Text>Número de Ruta: {this.state.selectedRoute.noruta}</Text>
                                <Text>Nombre de la Ruta: {this.state.selectedRoute.nombre}</Text>
                                <Text>Hora: {this.state.selectedRoute.hora}</Text>
                                <Text>Fecha de Inicio: {this.state.selectedRoute.fechainicio}</Text>
                                <Text>Fecha Final: {this.state.selectedRoute.fechafinal}</Text>
                                <Text>Conductor: {this.state.selectedRoute.conductor}</Text>
                                <Text>Conductor: {this.state.selectedRoute.conductor}</Text>
                                <Text>Transporte: {this.state.selectedRoute.transporte}</Text>
                                <Text>Parada: {this.state.selectedRoute.parada}</Text>
                                <TouchableOpacity style={styles.closeButton} onPress={this.closeModal}>
                                    <Text style={styles.buttonText}>Cerrar</Text>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
                    </Modal>
                )}

                  {/* Código para el modal de edición */}
                    {/* Modal de edición */}
                {/* Modal de edición */}
{/* Código para el modal de edición */}
{this.state.selectedRoute && (
                        <Modal
                        visible={this.state.showModalEdit}
                        transparent={true}
                        animationType="slide"
                    >
                        <View style={styles.modalContainer}>
                            <ScrollView style={styles.modalContent}>
                                <Text>Número de Ruta:</Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.editedRoute.noruta}
                                    onChangeText={value => this.handleInputChange('noruta', value)}
                                />
                                <Text>Nombre de la Ruta:</Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.editedRoute.nombre}
                                    onChangeText={value => this.handleInputChange('nombre', value)}
                                />
                                {/* Agregar más campos de edición aquí */}
                    
                                {/* Botón de guardar */}
                                <TouchableOpacity style={styles.saveButton} onPress={this.handleSaveEdit}>
                                    <Text style={styles.buttonText}>Guardar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.closeButton} onPress={this.closeModalEdit}>
                                    <Text style={styles.buttonText}>Cancelar</Text>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
                    </Modal>
                    
                )}

                {/* Código para el modal de añadir ruta */}
                {this.state.showModalAdd && (
    <Modal
        visible={this.state.showModalAdd}
        transparent={true}
        animationType="slide"
    >
        <View style={styles.modalContainer}>
            <ScrollView style={styles.modalContent}>
                {/* Formulario para la creación de una nueva ruta */}
                <Text>Número de Ruta:</Text>
                <TextInput
                    style={styles.input}
                    value={this.state.routeToAdd.noruta}
                    onChangeText={value => this.handleNewRouteChange('noruta', value)}
                />
                {/* Repite el proceso para los demás campos del formulario */}
                
                {/* Botón de guardar */}
                <TouchableOpacity style={styles.saveButton} onPress={this.handleSaveNewRoute}>
                    <Text style={styles.buttonText}>Guardar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.closeButton} onPress={this.closeModalAdd}>
                    <Text style={styles.buttonText}>Cancelar</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    </Modal>
)}
            </View>
        );
    }

    renderTable = (data) => {
        return (
            <View>
                {data.map(item => (
                    <View key={item.id} style={styles.tableRow}>
                        <Text style={styles.cell}>{item.noruta}</Text>
                        <Text style={styles.cell}>{item.nombre}</Text>
                        <View style={styles.actions}>
                            <TouchableOpacity style={styles.button} onPress={() => this.handleEdit(item)}>
                                <Text style={styles.buttonText}>Editar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={() => this.handleDelete(item)}>
                                <Text style={styles.buttonText}>Eliminar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={() => this.viewDetails(item)}>
                                <Text style={styles.buttonText}>Ver más</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                ))}
            </View>
        );
    }

    viewDetails = (item) => {
        this.setState({ showModal: true, selectedRoute: item });
    }

    closeModal = () => {
        this.setState({ showModal: false, selectedRoute: null });
    }

    handleInputChange = (field, value) => {
        // Actualizar el estado de la ruta editada
        this.setState(prevState => ({
            editedRoute: {
                ...prevState.editedRoute,
                [field]: value,
            },
        }));
    };

    handleSaveEdit = async () => {
        const { editedRoute, selectedRoute } = this.state;
        const { numeroconsecutivo } = selectedRoute;
    
        try {
            // Guardar los datos editados
            const response = await axios.put(`http://127.0.0.1:8000/tasks/api/v1/rutas/${numeroconsecutivo}/`, editedRoute);
            console.log('Datos editados guardados correctamente:', response.data);
    
            // Actualizar el estado local con los datos editados
            const updatedRoutes = this.state.routes.map(route => {
                if (route.numeroconsecutivo === numeroconsecutivo) {
                    return { ...route, ...editedRoute };
                }
                return route;
            });
            this.setState({ routes: updatedRoutes, showModalEdit: false }); // Update routes state
        } catch (error) {
            console.error('Error al guardar los datos editados:', error);
        }
    };
    
    

    closeModalEdit = () => {
        this.setState({ showModalEdit: false });
    };

    handleEdit = (item) => {
        // Configurar el estado para la ruta seleccionada
        this.setState({ selectedRoute: item, editedRoute: { ...item } });
        // Mostrar el modal de edición
        this.setState({ showModalEdit: true });
    };

    handleDelete = async (item) => {
        const { numeroconsecutivo } = item;
    
        try {
            // Make an API request to delete the item
            await axios.delete(`http://127.0.0.1:8000/tasks/api/v1/rutas/${numeroconsecutivo}/`);
    
            // Update the UI by removing the deleted item from the list of routes
            const updatedRoutes = this.state.routes.filter(route => route.numeroconsecutivo !== numeroconsecutivo);
            this.setState({ routes: updatedRoutes });
        } catch (error) {
            console.error('Error al eliminar el elemento:', error);
        }
    };


    // Función para manejar el evento de clic en el botón de crear nueva ruta
    handleAddRoute = () => {
        // Mostrar el modal de creación de una nueva ruta
        this.setState({ showModalAdd: true });
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    heading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    scrollView: {
        marginVertical: 10,
    },
    table: {
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 5,
        overflow: 'hidden',
    },
    tableRow: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#000',
    },
    cell: {
        flex: 1,
        padding: 10,
    },
    headerCell: {
        fontWeight: 'bold',
    },
    actions: {
        flexDirection: 'row',
    },
    button: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: 'blue',
        marginHorizontal: 5,
        borderRadius: 5,
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
        maxHeight: '80%',
    },
    closeButton: {
        marginTop: 10,
        padding: 10,
        backgroundColor: 'blue',
        borderRadius: 5,
        alignSelf: 'center',
    },
    saveButton: {
        backgroundColor: 'blue',
        borderRadius: 5,
        padding: 10,
    },
});

export default Rutas;
